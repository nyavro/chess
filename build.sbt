name := "chess"

version := "1.0"

scalaVersion := "2.11.8"


libraryDependencies ++= {
  Seq (
    "org.scalatest" % "scalatest_2.11" % "3.2.0-SNAP3"
  )
}