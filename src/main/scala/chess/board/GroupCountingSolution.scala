package chess.board

import scala.annotation.tailrec
import scala.collection.BitSet

/**
  * This solution is sophisticated version of OvercountingSolution. It's aim is to provide smarter way of counting positions
  * without need to 'normalize' it afterwards by dividing on appearance count factorials and to reduce combinations to check
  *
  * The idea is:
  *   1. Process groups of the same pieces
  *   2. Inside the group provide the order of run over to exclude the duplicates in count
  */
class GroupCountingSolution(board:Board) {

  class Status(occupied:Map[Piece, BitSet]) {

    @tailrec
    private def putAll(target:List[(Int,Status)], piece:Piece, count:Int):List[(Int,Status)] = count match {
      case 0 => target
      case n =>
        putAll(
          target.flatMap {
            case (index, status) => status.put(piece, index)
          },
          piece,
          count-1
        )
    }

    def put(piece:Piece, minIndex:Int):List[(Int, Status)] = {
      val available = board.fullOccupied ^ occupied(piece)
      available
        .dropWhile(_<minIndex)
        .map {
          index => {
            val newlyAttacked = piece.putAtIndex(board, index)
            (index, new Status(occupied.map {case (k, beforePlacement) => k -> (k.putAtIndex(board, index) | beforePlacement | newlyAttacked)}))
          }
        }
        .toList
    }

    def count(groups:List[(Piece, Int)]):Long = groups match {
      //    case (piece, count)::Nil => put4(List((0, this)), piece, count-1).map(v => board.size - v._2.occupied(piece).size).sum
      case (piece, count)::xs => putAll(List((0, this)), piece, count).map(_._2.count(xs)).sum
      case _ => 1
    }
  }

  def count(pieces:List[Piece]) =
    new Status(pieces.map(p => p-> BitSet()).toMap)
      .count(
        pieces
          .sortBy(_.rank)
          .groupBy(identity)
          .toList
          .map {
            case (p, v) =>(p,v.size)
          }
          .sortBy(_._1.rank)
      )
}

