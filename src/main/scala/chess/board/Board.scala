package chess.board

import scala.collection.BitSet

case class Board(rows:Int, cols:Int) {
  val fullOccupied = BitSet(0 until rows*cols:_*)
  val size = rows*cols
}

trait Piece {
  def rank:Int

  /**
    * Returns mask of occupied fields of the board
    * @param board - dimens of the chess board
    * @param row - row to put piece to
    * @param col - column to put piece to
    * @return BitSet of occupied positions
    */
//  TODO: introduce cache to not to compute masks at every call
  def putOnBoard(board:Board, row:Int, col:Int):BitSet

  /**
    * Places element to board at index.
    * @param board - dimens of the chess board
    * @param index - Index. Indexes goes from left to right, up to down, (0,0) field has index 0
    * @return BitSet of occupied positions
    */
  def putAtIndex(board:Board, index:Int):BitSet = putOnBoard(board, index/board.cols, index%board.cols)
}

case object Queen extends Piece {
  val rank = 0
  def putOnBoard(board:Board, row:Int, col:Int):BitSet = {
    Rook.putOnBoard(board, row, col) | Bishop.putOnBoard(board, row, col)
  }
}

case object Rook extends Piece {
  val rank = 1
  def putOnBoard(board:Board, row:Int, col:Int):BitSet = {
    val indexes = (0 until board.rows).map(i => i*board.cols + col) ++ (0 until board.cols).map(j => row*board.cols + j)
    BitSet(indexes:_*)
  }
}

case object Bishop extends Piece {
  val rank = 2
  def putOnBoard(board:Board, row:Int, col:Int):BitSet = {
    val lt = row.min(col)
    val rb = (board.rows-row).min(board.cols-col)
    val lb = (col+1).min(board.rows-row)-1
    val rt = (row+1).min(board.cols-col)
    val indexes =
      (-lt until rb).map (v => (row+v)*board.cols+col+v) ++
        (-lb until rt).map (v => (row-v)*board.cols+col+v)
    BitSet(indexes:_*)
  }
}

case object King extends Piece {
  val rank = 3
  def putOnBoard(board:Board, row:Int, col:Int):BitSet = {
    val indexes = for {
      i <- (row-1).max(0) to (row+1).min(board.rows-1)
      j <- (col-1).max(0) to (col+1).min(board.cols-1)
    } yield i*board.cols + j
    BitSet(indexes:_*)
  }
}

case object Knight extends Piece {
  val rank = 4
  def putOnBoard(board:Board, row:Int, col:Int):BitSet = {
    val indexes = List((row, col),(row-2, col-1), (row-2, col+1), (row-1, col-2), (row-1, col+2), (row+1, col-2), (row+1, col+2), (row+2, col-1), (row+2, col+1))
      .withFilter {
        case (r, c) => r>=0 && c>=0 && r<board.rows && c<board.cols
      }.map {
      case (r,c) => r*board.cols + c
    }
    BitSet(indexes:_*)
  }
}

object Chess {

  def main(args: Array[String]): Unit = {
    val pieces = List(Knight, Knight, Knight, Knight,Bishop, Bishop)
    val board = Board(4, 3)
    println("Possible positions:" + new GroupCountingSolution(board).count(pieces))
  }
}