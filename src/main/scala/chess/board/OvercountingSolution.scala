package chess.board

import scala.collection.BitSet

/**
  * This was the first solution, containing some problem. It works correct only for sequences having no repeating items,
  * i.e. List(King, Knight, Rook). If input contains duplicates, then the solution 'overcalculates' variants.
  * So for input of pieces n1, n2,...nk having counts c1, c2,...ck correspondently the correct solution would be:
  * count = overCount / (c1! * c2! * ... * ck!)
  *
  * The idea of the algorithm is the following.
  * 1. Sort input pieces by it's rank - to process heavy pieces first. This will help to reduce the number of combinations
  *   to check. The desired order of processing is Queen, Rook, Bishop, King, Knight
  * 2. For each piece seen in input list of pieces we're going to store unavailable fields - fields under strike or
  *   already occupied by the piece
  * 3. When piece placed on the board then for each kind of piece should be updated their unavailable fields.
  *   For example, consider 4x4 board. If we place the Bishop at field with row=1 and column=0 then fields unavailable fields
  *   for Bishop type are:
  *   Bishop's available/unavailable fields
  *                                                - * - -
  *                                                B - - -
  *                                                - * - -
  *                                                - - * -
  *   So it is possible to place another Bishop at any point marked with '-'. It can be seen that field with row=0 and
  *   column=2 is available for Bishop, whereas this position is prohibited for Knight:
  *   Knight's available/unavailable fields
  *                                                - * * -
  *                                                * - - -
  *                                                - * * -
  *                                                - * * -
  * 4. At each piece placement, the updated mask of each kind of piece formed from: mask before placement + newly attacked
  *   positions + reverse attack mask
  *
  *
*/

class OvercountingSolution(board:Board) {

  class Status(occupied:Map[Piece, BitSet]) {
    private def put(piece:Piece):List[Status] = {
      val available = board.fullOccupied ^ occupied(piece)
      available
        .map {
          index => {
            val newlyAttacked = piece.putAtIndex(board, index)
            new Status(occupied.map {case (k, beforePlacement) => k -> (k.putAtIndex(board, index) | beforePlacement | newlyAttacked)})
          }
        }
        .toList
    }

    def count(pieces:List[Piece]):Long = pieces match {
      case Nil => 1
      case x::xs => put(x).map(_.count(xs)).sum
    }
  }

  def count(pieces:List[Piece]) = new Status(pieces.map(p => p-> BitSet()).toMap).count(pieces.sortBy(_.rank))
}

