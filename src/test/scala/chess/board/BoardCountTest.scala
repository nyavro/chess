package chess.board

import org.scalatest.{FlatSpec, Matchers}

class BoardCountTest extends FlatSpec with Matchers {

  "ChessCounter" should "count variants of placing one Knight on a board" in {
    val pieces = List(Knight)
    new GroupCountingSolution(Board(1, 1)).count(pieces) should be (1)
    new GroupCountingSolution(Board(1, 2)).count(pieces) should be (2)
    new GroupCountingSolution(Board(1, 3)).count(pieces) should be (3)
    new GroupCountingSolution(Board(2, 3)).count(pieces) should be (6)
    new GroupCountingSolution(Board(3, 3)).count(pieces) should be (9)
    new GroupCountingSolution(Board(4, 3)).count(pieces) should be (12)
    new GroupCountingSolution(Board(4, 4)).count(pieces) should be (16)
  }

  "ChessCounter" should "count variants of placing 2 Knights on a board" in {
    val pieces = List(Knight, Knight)
    new GroupCountingSolution(Board(1, 1)).count(pieces) should be (0)
    new GroupCountingSolution(Board(1, 2)).count(pieces) should be (1)
    new GroupCountingSolution(Board(1, 3)).count(pieces) should be (3)
    new GroupCountingSolution(Board(2, 3)).count(pieces) should be (13)
    new GroupCountingSolution(Board(3, 3)).count(pieces) should be (28)
    new GroupCountingSolution(Board(4, 3)).count(pieces) should be (52)
    new GroupCountingSolution(Board(4, 4)).count(pieces) should be (96)
  }

  "ChessCounter" should "count variants of placing 3 Knights on a board" in {
    val pieces = List(Knight, Knight, Knight)
    new GroupCountingSolution(Board(1, 1)).count(pieces) should be (0)
    new GroupCountingSolution(Board(1, 2)).count(pieces) should be (0)
    new GroupCountingSolution(Board(1, 3)).count(pieces) should be (1)
    new GroupCountingSolution(Board(2, 3)).count(pieces) should be (12)
    new GroupCountingSolution(Board(3, 3)).count(pieces) should be (36)
  }

  "ChessCounter" should "count variants of placing 4 Knights and two Bishops on a board" in {
    val pieces = List(Knight, Knight, Knight, Knight, Bishop, Bishop)
    new GroupCountingSolution(Board(4, 3)).count(pieces) should be (15)
    new GroupCountingSolution(Board(4, 4)).count(pieces) should be (372)
    new GroupCountingSolution(Board(4, 5)).count(pieces) should be (3904)
  }

  "ChessCounter" should "count variants of placing 2 Knights and 2 Bishops and 2 Rooks on a board" in {
    val pieces = List(Knight, Knight, Bishop, Bishop, Rook, Rook)
    new GroupCountingSolution(Board(4, 4)).count(pieces) should be (12)
    new GroupCountingSolution(Board(4, 5)).count(pieces) should be (254)
  }
}
