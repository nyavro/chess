package chess.board

import org.scalatest.{FlatSpec, Matchers}

class BishopCheck extends FlatSpec with Matchers {
  "Bishop" should "strike main diagonal" in {
    val notAvailableAnyMore = Bishop.putOnBoard(Board(4, 5), 0, 0)
    notAvailableAnyMore.size should be (4)
    notAvailableAnyMore should contain (0)
    notAvailableAnyMore should contain (6)
    notAvailableAnyMore should contain (12)
    notAvailableAnyMore should contain (18)
  }
  "Bishop" should "strike aux diagonal" in {
    val notAvailableAnyMore = Bishop.putOnBoard(Board(4, 5), 0, 4)
    notAvailableAnyMore.size should be (4)
    notAvailableAnyMore should contain (4)
    notAvailableAnyMore should contain (8)
    notAvailableAnyMore should contain (12)
    notAvailableAnyMore should contain (16)
  }
  "Bishop" should "strike both diagonals" in {
    val notAvailableAnyMore = Bishop.putOnBoard(Board(4, 5), 2, 1)
    notAvailableAnyMore.size should be (6)
    notAvailableAnyMore should contain (3)
    notAvailableAnyMore should contain (5)
    notAvailableAnyMore should contain (7)
    notAvailableAnyMore should contain (11)
    notAvailableAnyMore should contain (15)
    notAvailableAnyMore should contain (17)
  }
  "Bishop" should "pass check" in {
    val notAvailableAnyMore = Bishop.putOnBoard(Board(4, 6), 3, 3)
    notAvailableAnyMore.size should be (6)
    notAvailableAnyMore should contain (0)
    notAvailableAnyMore should contain (7)
    notAvailableAnyMore should contain (14)
    notAvailableAnyMore should contain (21)
    notAvailableAnyMore should contain (16)
    notAvailableAnyMore should contain (11)
  }
}