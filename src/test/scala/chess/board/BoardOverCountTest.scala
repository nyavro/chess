package chess.board

import org.scalatest.{FlatSpec, Matchers}

class BoardOverCountTest extends FlatSpec with Matchers {

  "ChessCounter" should "count variants of placing one Knight on a board" in {
    val pieces = List(Knight)
    new OvercountingSolution(Board(1, 1)).count(pieces) should be (1)
    new OvercountingSolution(Board(1, 2)).count(pieces) should be (2)
    new OvercountingSolution(Board(1, 3)).count(pieces) should be (3)
    new OvercountingSolution(Board(2, 3)).count(pieces) should be (6)
    new OvercountingSolution(Board(3, 3)).count(pieces) should be (9)
    new OvercountingSolution(Board(4, 3)).count(pieces) should be (12)
    new OvercountingSolution(Board(4, 4)).count(pieces) should be (16)
  }

  "ChessCounter" should "count variants of placing 2 Knights on a board" in {
    val pieces = List(Knight, Knight)
    new OvercountingSolution(Board(1, 1)).count(pieces)/2 should be (0)
    new OvercountingSolution(Board(1, 2)).count(pieces)/2 should be (1)
    new OvercountingSolution(Board(1, 3)).count(pieces)/2 should be (3)
    new OvercountingSolution(Board(2, 3)).count(pieces)/2 should be (13)
    new OvercountingSolution(Board(3, 3)).count(pieces)/2 should be (28)
    new OvercountingSolution(Board(4, 3)).count(pieces)/2 should be (52)
    new OvercountingSolution(Board(4, 4)).count(pieces)/2 should be (96)
  }

  "ChessCounter" should "count variants of placing 3 Knights on a board" in {
    val pieces = List(Knight, Knight, Knight)
    new OvercountingSolution(Board(1, 1)).count(pieces)/6 should be (0)
    new OvercountingSolution(Board(1, 2)).count(pieces)/6 should be (0)
    new OvercountingSolution(Board(1, 3)).count(pieces)/6 should be (1)
    new OvercountingSolution(Board(2, 3)).count(pieces)/6 should be (12)
    new OvercountingSolution(Board(3, 3)).count(pieces)/6 should be (36)
  }

  "ChessCounter" should "count variants of placing 4 Knights and two Bishops on a board" in {
    val pieces = List(Knight, Knight, Knight, Knight, Bishop, Bishop)
    new OvercountingSolution(Board(4, 3)).count(pieces)/48 should be (15)
    new OvercountingSolution(Board(4, 4)).count(pieces)/48 should be (372)
    new OvercountingSolution(Board(4, 5)).count(pieces)/48 should be (3904)
  }

  "ChessCounter" should "count variants of placing 2 Knights and 2 Bishops and 2 Rooks on a board" in {
    val pieces = List(Knight, Knight, Bishop, Bishop, Rook, Rook)
    new OvercountingSolution(Board(4, 4)).count(pieces)/8 should be (12)
    new OvercountingSolution(Board(4, 5)).count(pieces)/8 should be (254)
  }
}
