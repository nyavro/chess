package chess.board

import org.scalatest.{FlatSpec, Matchers}

class RookCheck extends FlatSpec with Matchers {
  "Rook" should "strike first row and column" in {
    val notAvailableAnyMore = Rook.putOnBoard(Board(3, 5), 0, 0)
    notAvailableAnyMore.size should be (7)
    notAvailableAnyMore should contain (0)
    notAvailableAnyMore should contain (4)
    notAvailableAnyMore should contain (5)
    notAvailableAnyMore should contain (10)
  }
  "Rook" should "strike row and column" in {
    val notAvailableAnyMore = Rook.putOnBoard(Board(3, 4), 1, 1)
    notAvailableAnyMore.size should be (6)
    notAvailableAnyMore should contain (1)
    notAvailableAnyMore should contain (4)
    notAvailableAnyMore should contain (5)
    notAvailableAnyMore should contain (7)
    notAvailableAnyMore should contain (9)
  }
}