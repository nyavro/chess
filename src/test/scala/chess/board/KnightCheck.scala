package chess.board

import org.scalatest.{FlatSpec, Matchers}

class KnightCheck extends FlatSpec with Matchers {
  "Knight" should "strike two fields when in corner" in {
    val notAvailableAnyMore = Knight.putOnBoard(Board(3, 5), 0, 0)
    notAvailableAnyMore.size should be (3)
    notAvailableAnyMore should contain (0)
    notAvailableAnyMore should contain (7)
    notAvailableAnyMore should contain (11)
  }
  "Knight" should "strike four fields when on side" in {
    val notAvailableAnyMore = Knight.putOnBoard(Board(9, 10), 4, 9)
    notAvailableAnyMore.size should be (5)
  }
  "Knight" should "strike eight fields when enough space" in {
    val notAvailableAnyMore = Knight.putOnBoard(Board(9, 9), 4, 4)
    notAvailableAnyMore.size should be (9)
  }
}

