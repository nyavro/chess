
package chess.board

import org.scalatest.{FlatSpec, Matchers}

class KingCheck extends FlatSpec with Matchers {
  "King" should "strike four fields when in corner" in {
    val notAvailableAnyMore = King.putOnBoard(Board(3, 5), 0, 0)
    notAvailableAnyMore.size should be (4)
  }
  "King" should "strike six fields when on side" in {
    val notAvailableAnyMore = King.putOnBoard(Board(5, 10), 3, 9)
    notAvailableAnyMore.size should be (6)
  }
  "King" should "strike eight fields" in {
    val notAvailableAnyMore = King.putOnBoard(Board(5, 10), 3, 3)
    notAvailableAnyMore.size should be (9)
  }
}